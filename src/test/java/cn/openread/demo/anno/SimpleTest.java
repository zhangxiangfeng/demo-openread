package cn.openread.demo.anno;

import cn.openread.demo.anno.demo.Column;
import cn.openread.demo.anno.demo.Filter;
import cn.openread.demo.anno.demo.Table;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * demo
 * <p>
 * 源码注解
 * 编译注解
 * 运行注解
 *
 * @author simon
 * @create 2018-06-15 上午9:48
 **/
public class SimpleTest {

    private static String query(Filter filter) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        StringBuilder sql = new StringBuilder();

        //1.加载class
        Class<? extends Filter> clazz = filter.getClass();
        if (!clazz.isAnnotationPresent(Table.class)) {
            return null;
        }

        //2.获取表名
        Table table = clazz.getAnnotation(Table.class);
        String tableName = table.value();
        sql.append("select * from ").append(tableName).append(" ").append("where 1=1");

        //3.循环所有字段
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (!field.isAnnotationPresent(Column.class)) {
                continue;
            }

            Column column = field.getAnnotation(Column.class);
            String columnName = column.value();
            String filedName = field.getName();

            String getMethodName = "get" + filedName.substring(0, 1).toUpperCase() + filedName.substring(1);

            Method getMethod = clazz.getMethod(getMethodName);
            Object fildValue = getMethod.invoke(filter);

            if (null == fildValue) {
                continue;
            }
            sql.append(" and ");
            if (fildValue instanceof String) {
                if (((String) fildValue).contains(",") && ((String) fildValue).split(",").length > 1) {
                    String[] strings = ((String) fildValue).split(",");

                    sql.append(columnName).append(" in (");

                    for (int i = 0; i < strings.length; i++) {
                        sql.append("'").append(strings[i]).append("'");
                        if (i + 1 != strings.length) {
                            sql.append(",");
                        }
                    }

                    sql.append(")");
                } else
                    sql.append(columnName).append("=").append("'").append(fildValue).append("'");
            } else
                sql.append(columnName).append("=").append(fildValue);

        }
        return sql.toString();
    }

    @SuppressWarnings({"deprecation", "Duplicates"})
    @Test
    public void testAnno() throws ClassNotFoundException {
        Person person = new Child();
        person.sing();

        //1.加载类
        Class clazz = Class.forName("cn.openread.demo.anno.Child");

        //2.1 在类上找到注解,并且读取注解
        if (clazz.isAnnotationPresent(Description.class)) {
            Description description = (Description) clazz.getAnnotation(Description.class);
            System.out.print(description.age());
            System.out.print(description.desc());
            System.out.println(description.author());
        } else {
            System.out.println("IN CLASS NOT FOUND");
        }

        Method[] methods = clazz.getMethods();

        //2.2 在方法上找注解
        for (Method method : methods) {
            if (method.isAnnotationPresent(Description.class)) {
                Description description = (Description) clazz.getAnnotation(Description.class);
                System.out.print(description.age());
                System.out.print(description.desc());
                System.out.println(description.author());
            }
        }


        //3.另外的解析方法
        for (Method method : methods) {
            Annotation[] annotations = method.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof Description) {
                    Description description = (Description) clazz.getAnnotation(Description.class);
                    System.out.println(description.age());
                    System.out.println(description.desc());
                    System.out.println(description.author());
                }
            }

        }
    }

    @Test
    public void testDemo() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Filter f1 = new Filter();
        f1.setId(10L);

        Filter f2 = new Filter();
        f2.setUserName("simon");
        f2.setId(20L);

        Filter f3 = new Filter();
        f3.setEmail("812135023@qq.comm");

        System.out.println(query(f1));
        System.out.println(query(f2));
        System.out.println(query(f3));
    }
}
