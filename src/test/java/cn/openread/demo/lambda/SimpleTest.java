package cn.openread.demo.lambda;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author simon
 * @create 2018-08-22 下午4:01
 **/
public class SimpleTest {


    @Test
    public void test2() throws InterruptedException {
        new Thread(() -> System.out.println("Hello World!")).start();
        TimeUnit.SECONDS.sleep(1000);
    }

    @Test
    public void test() throws InterruptedException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Hello World!");
            }
        }).start();
        TimeUnit.SECONDS.sleep(1000);
    }


}
