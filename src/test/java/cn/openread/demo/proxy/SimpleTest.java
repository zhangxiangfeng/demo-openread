package cn.openread.demo.proxy;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * test
 *
 * @author simon
 * @create 2018-05-22 上午10:24
 **/
public class SimpleTest {

    @Test
    public void test() throws UnsupportedEncodingException {
        String content = "transName=TP08&Plain=MerchantId%3D4%7CMerUserId%3D4%7CMerUserAcctNo%3D030129000000119500000185%7CMerSellerId%3D4%7CMerSellerAcctNo%3D030129000000119500000181%7CVerCode%3D%7CVerSeqNo%3D%7CMerchantSeqNo%3D04bb76c18c7a4990935dbc6d79378532%7CMerchantDateTime%3D20180522094700%7COperType%3D101%7COrgMerchantSeqNo%3D%7CPayDevide%3D1%7CFeeAmount%3D%7CTransAmount%3D0.01%7CCurrency%3D156%7COrderNo%3D04bb76c18c7a4990935dbc6d79378532%7CProductInfo%3Dtest%7CRemark1%3D%7CRemark2%3D%7C&Signature=643b833b016d233a2330d944ea24cb236d199962eab6fd22acbf170bc45e9e4d91ceafaf22263a2374ee32880f0dc4ea5bfbd971617fbc89d2a4d828f1cfa3e1912c2b137b0f57dc481c8884cb034e71cef60c7169a8361c25c26318c05284eaaa844c43e2c60396a5ff1416b4f0722830645eb8022b9cda81b02c6c7e4f6379";
        String encode = "utf-8";
        String ss = URLDecoder.decode(content, encode);
        for (String s : ss.split("\\|")) {
            System.out.println(s);
        }

    }

    @Test
    public void test22() {
        System.out.println(100 % 3);
    }
}
