package cn.openread.demo.proxy;

import cn.openread.demo.proxy.cglib.CglibProxy;
import cn.openread.demo.proxy.cglib.SayHello;
import cn.openread.demo.proxy.deep.MyClassLoader;
import cn.openread.demo.proxy.jdk.CustomInvocationHandler;
import cn.openread.demo.proxy.jdk.HelloWorld;
import cn.openread.demo.proxy.jdk.HelloWorldImpl;
import net.sf.cglib.core.DebuggingClassWriter;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;


public class ProxyTest {

    /**
     * @link http://blog.csdn.net/mhmyqn/article/details/48474815(细说JDK动态代理的实现原理)
     * @link https://blog.csdn.net/luanlouis/article/details/24589193
     * <p>
     * spring-aop代理
     * <p>
     * 实现方式:1.jdk动态代理
     * <p>
     * 1.继承了Proxy类，实现了代理的接口，由于java不能多继承，这里已经继承了Proxy类了，不能再继承其他的类，所以JDK的动态代理不支持对实现类的代理，只支持接口的代理。
     * 2.提供了一个使用InvocationHandler作为参数的构造方法。
     * 3.生成静态代码块来初始化接口中方法的Method对象，以及Object类的equals、hashCode、toString方法。
     * 4.重写了Object类的equals、hashCode、toString，它们都只是简单的调用了InvocationHandler的invoke方法，即可以对其进行特殊的操作，也就是说JDK的动态代理还可以代理上述三个方法。
     * 5.代理类实现代理接口的sayHello方法中，只是简单的调用了InvocationHandler的invoke方法，我们可以在invoke方法中进行一些特殊操作，甚至不调用实现的方法，直接返回。
     */
    @Test
    public void jdkProxyTest() {
//        java.security.AccessController.doPrivileged(new GetBooleanAction("jdk.proxy.ProxyGenerator.saveGeneratedFiles"));
        //jdk9
        System.getProperties().put("jdk.proxy.ProxyGenerator.saveGeneratedFiles", "true");
//        //jdk7
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        CustomInvocationHandler handler = new CustomInvocationHandler(new HelloWorldImpl());

        HelloWorld proxy = (HelloWorld) Proxy.newProxyInstance(
                ProxyTest.class.getClassLoader(),
                new Class[]{HelloWorld.class},
                handler);

        proxy.sayHello("Simon");

//        proxy.exit("Simon");
//
//        proxy.toString();
//        proxy.hashCode();
//        proxy.equals(null);
    }


    /**
     * @link http://blog.csdn.net/yakoo5/article/details/9099133(CGLib动态代理原理及实现)
     * <p>
     * 1.CGLib创建的动态代理对象性能比JDK创建的动态代理对象的性能高不少，但是CGLib在创建代理对象时所花费的时间却比JDK多得多，
     * 所以对于单例的对象，因为无需频繁创建对象，用CGLib合适，反之，使用JDK方式要更为合适一些。
     * 2.同时，由于CGLib由于是采用动态创建子类的方法，对于final方法，无法进行代理。
     * 3.对于没有接口的类，如何实现动态代理呢，这就需要CGLib了。
     * 4.CGLib采用了非常底层的字节码技术，其原理是通过字节码技术为一个类创建子类
     */
    @Test
    public void cglibProxyTest() {
        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "/media/simon/data/software/projects/labs/java/demo-openread/out");

        CglibProxy proxy = new CglibProxy();
        //通过生成子类的方式创建代理类
        SayHello proxyImp = (SayHello) proxy.getProxy(SayHello.class);
        proxyImp.say();
    }


    /**
     * 下面通过一段代码演示手动加载 class文件字节码到系统内，转换成class对象，然后再实例化的过程
     */
    @Test
    public void testDiyGenClass() throws IOException, IllegalAccessException, InstantiationException {
        //读取本地的class文件内的字节码，转换成字节码数组
        File file = new File(".");
        InputStream input = new FileInputStream(file.getCanonicalPath() + "/src/main/java/cn/openread/demo/proxy/deep/Programmer.class");
        byte[] result = new byte[1024];

        int count = input.read(result);
        // 使用自定义的类加载器将 byte字节码数组转换为对应的class对象
        MyClassLoader loader = new MyClassLoader();
        Class clazz = loader.defineMyClass(result, 0, count);
        //测试加载是否成功，打印class 对象的名称
        System.out.println(clazz.getCanonicalName());

        //实例化一个Programmer对象
        Object o = clazz.newInstance();
        try {
            //调用Programmer的code方法
            clazz.getMethod("code", null).invoke(o, null);
        } catch (IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        }
    }
}
