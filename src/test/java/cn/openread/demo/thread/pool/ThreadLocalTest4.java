package cn.openread.demo.thread.pool;

/**
 * ThreadLocalTest
 *
 * @author simon
 * @create 2018-08-16 下午3:22
 **/
public class ThreadLocalTest4 {

    ThreadLocal<Long> longLocal = new ThreadLocal<Long>() {
        protected Long initialValue() {
            return Thread.currentThread().getId();
        }
    };
    ThreadLocal<String> stringLocal = new ThreadLocal<String>() {
        ;

        protected String initialValue() {
            return Thread.currentThread().getName();
        }
    };

    public static void main(String[] args) throws InterruptedException {
        final ThreadLocalTest4 test = new ThreadLocalTest4();

        System.out.println(test.getLong());
        System.out.println(test.getString());

        Thread thread1 = new Thread() {
            public void run() {
                test.set();
                System.out.println(test.getLong());
                System.out.println(test.getString());
            }

            ;
        };
        thread1.start();
        thread1.join();

        System.out.println(test.getLong());
        System.out.println(test.getString());
    }

    public void set() {
        longLocal.set(Thread.currentThread().getId());
        stringLocal.set(Thread.currentThread().getName());
    }

    public long getLong() {
        return longLocal.get();
    }

    public String getString() {
        return stringLocal.get();
    }
}
