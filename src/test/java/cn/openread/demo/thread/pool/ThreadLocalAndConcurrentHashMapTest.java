package cn.openread.demo.thread.pool;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ThreadLocalTest
 * <p>
 * ConcurrentHashMap 是基于主线程
 * ThreadLocal 基于当前线程
 *
 * @author simon
 * @create 2018-08-16 下午3:22
 **/
public class ThreadLocalAndConcurrentHashMapTest {

    private static ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
    private static ThreadLocal<Object> threadLocal = new ThreadLocal<Object>() {

        @Override
        protected Object initialValue() {
            return "初始化值";
        }

    };

    public static void main(String[] args) {
        new Thread("Thread1") {
            @Override
            public void run() {
                threadLocal.set(Thread.currentThread().getName() + "Thread1");
                map.put(Thread.currentThread().getName(), Thread.currentThread().getName());
            }
        }.start();


        new Thread("Thread2") {
            @Override
            public void run() {
                threadLocal.set(Thread.currentThread().getName() + "Thread2");
                System.out.println(Thread.currentThread().getName() + "===" + threadLocal.get());
                map.put(Thread.currentThread().getName(), Thread.currentThread().getName());
            }
        }.start();

        new Thread("Thread3") {
            @Override
            public void run() {
                threadLocal.set(Thread.currentThread().getName() + "Thread3");
                map.put(Thread.currentThread().getName(), Thread.currentThread().getName());
            }
        }.start();

        System.out.println("111-" + Thread.currentThread().getName() + "===" + threadLocal.get());

        for (Map.Entry<String, String> item : map.entrySet()) {
            System.out.println(item.getKey() + "===" + item.getValue());
        }
    }
}
