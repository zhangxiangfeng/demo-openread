package cn.openread.demo.thread.pool;

import org.junit.Test;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 面试-线程池的成长之路
 * <p>
 * 线程的拒绝策略
 * AbortPolicy策略：该策略会直接抛出异常，阻止系统正常工作。
 * CallerRunsPolicy 策略：只要线程池未关闭，该策略直接在调用者线程中，运行当前的被丢弃的任务。
 * DiscardOleddestPolicy策略： 该策略将丢弃最老的一个请求，也就是即将被执行的任务，并尝试再次提交当前任务
 * DiscardPolicy策略：该策略默默的丢弃无法处理的任务，不予任何处理。
 * <p>
 * 除了JDK默认为什么提供的四种拒绝策略，我们可以根据自己的业务需求去自定义拒绝策略，自定义的方式很简单，直接实现RejectedExecutionHandler接口即可
 * <p>
 * 比如Spring integration中就有一个自定义的拒绝策略CallerBlocksPolicy，将任务插入到队列中，直到队列中有空闲并插入成功的时候，否则将根据最大等待时间一直阻塞，直到超时。
 * <p>
 * https://mp.weixin.qq.com/s?__biz=MzI4NDY5Mjc1Mg==&mid=2247484869&idx=1&sn=d5ded7f08efe2e1d1682bea7aa1f8263&chksm=ebf6ddbadc8154acb7e52e2f35b1b6bcad63655748863bd09f9c1cf37f3141e4e9bc18acba87&mpshare=1&scene=1&srcid=0524VtcPKAa0tNb6VuUHRLyA#rd
 */
public class SimpleTest {

    public static void main(String[] args) throws InterruptedException {
        SimpleTest test = new SimpleTest();
//        test.testNewSingleThreadExecutor();
//        test.testNewFixedThreadPool();
//        test.testNewCachedThreadPool();
//        test.testNewScheduledThreadPool();
//        test.testNewWorkStealingPool();
//        test.testShutdownNow();
//        test.testShutDown1();
        test.testShutDown2();
    }

    /**
     * 一个单线程的线程池。这个线程池只有一个线程在工作，也就是相当于单线程串行执行所有任务。
     * 如果这个唯一的线程因为异常结束，那么会有一个新的线程来替代它。
     * 此线程池保证所有任务的执行顺序按照任务的提交顺序执行。
     */
    private void testNewSingleThreadExecutor() {
        System.out.println("测试-> testNewSingleThreadExecutor");
        ExecutorService pool = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 10; i++) {
            pool.execute(() -> System.out.println(Thread.currentThread().getName() + "开始发车啦...."));
        }
    }

    /**
     * 创建固定大小的线程池。每次提交一个任务就创建一个线程，直到线程达到线程池的最大大小。
     * 线程池的大小一旦达到最大值就会保持不变，
     * 如果某个线程因为执行异常而结束，那么线程池会补充一个新线程。
     * <p>
     * 这里模拟10个任务,给线程池去运行
     */
    private void testNewFixedThreadPool() {
        System.out.println("测试-> testNewFixedThreadPool");
        ExecutorService pool = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            pool.execute(() -> System.out.println(Thread.currentThread().getName() + "开始发车啦...."));
        }
    }

    /**
     * 创建一个可缓存的线程池。如果线程池的大小超过了处理任务所需要的线程
     * ，那么就会回收部分空闲的线程，当任务数增加时，此线程池又添加新线程来处理任务。
     * <p>
     * 这里模拟10个任务,给线程池去运行
     */
    private void testNewCachedThreadPool() {
        System.out.println("测试-> testNewCachedThreadPool");
        ExecutorService pool = Executors.newCachedThreadPool();
        for (int i = 0; i < 200; i++) {
            pool.execute(() -> System.out.println(Thread.currentThread().getName() + "开始发车啦...."));
        }
    }

    /**
     * 此线程池支持定时以及周期性执行任务的需求。
     * <p>
     * 这里模拟10个任务,给线程池去运行
     */
    private void testNewScheduledThreadPool() {
        System.out.println("测试-> newScheduledThreadPool");
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(10);
        for (int i = 0; i < 10; i++) {
            pool.schedule(() -> System.out.println(Thread.currentThread().getName() + "开始发车啦...."), 1, TimeUnit.SECONDS);
        }
    }

    /**
     * newWorkStealingPool是jdk1.8才有的，会根据所需的并行层次来动态创建和关闭线程，
     * 通过使用多个队列减少竞争，底层用的ForkJoinPool来实现的。ForkJoinPool的优势在于，可以充分利用多cpu，多核cpu的优势，
     * 把一个任务拆分成多个“小任务”，把多个“小任务”放到多个处理器核心上并行执行；当多个“小任务”执行完成之后，再将这些执行结果合并起来即可。
     * <p>
     * 这里模拟10个任务,给线程池去运行
     */
    private void testNewWorkStealingPool() {
        System.out.println("测试-> newWorkStealingPool");
        ExecutorService pool = Executors.newWorkStealingPool();
        for (int i = 0; i < 10; i++) {
            pool.execute(() -> System.out.println(Thread.currentThread().getName() + "开始发车啦...."));
        }

        List<Runnable> runs = pool.shutdownNow();
    }

    /**
     * 在前面的讲解中，我们执行任务是用的execute方法，除了execute方法，还有一个submit方法也可以执行我们提交的任务。
     * <p>
     * 这两个方法有什么区别呢
     * 1.execute适用于不需要关注返回值的场景，只需要将线程丢到线程池中去执行就可以了
     * 2.submit方法适用于需要关注返回值的场景
     */
    @Test
    public void testPolicy() throws ExecutionException, InterruptedException {
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(100);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                10, 100, 10, TimeUnit.SECONDS, workQueue, new ThreadPoolExecutor.CallerRunsPolicy());
        executor.execute(() -> System.out.println(1));
        Future<?> submit = executor.submit(() -> {
            int a = 0 + 10;
            return a;
        });
        System.out.println(submit.get());
    }

    /**
     * 直接submit一个Runnable是拿不到返回值的，返回值就是null.
     */
    @Test
    public void test() throws ExecutionException, InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        Data data = new Data();
        Future<Data> future = pool.submit(new MyRunnable(data), data);
        String result = future.get().getName();
        System.out.println(result);
    }

    /**
     * shutdownNow：对正在执行的任务全部发出interrupt()，停止执行，对还未开始执行的任务全部取消，并且返回还没开始的任务列表
     */
    private void testShutdownNow() throws InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(1);
        for (int i = 0; i < 5; i++) {
            System.err.println(i);
            pool.execute(() -> {
                try {
                    Thread.sleep(30000);
                    System.out.println("--");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        Thread.sleep(1000);
        List<Runnable> runs = pool.shutdownNow();
    }

    /**
     * 五种线程池的使用场景
     * <p>
     * newSingleThreadExecutor：一个单线程的线程池，可以用于需要保证顺序执行的场景，并且只有一个线程在执行。
     * <p>
     * newFixedThreadPool：一个固定大小的线程池，可以用于已知并发压力的情况下，对线程数做限制。
     * <p>
     * newCachedThreadPool：一个可以无限扩大的线程池，比较适合处理执行时间比较小的任务。
     * <p>
     * newScheduledThreadPool：可以延时启动，定时启动的线程池，适用于需要多个后台线程执行周期任务的场景。
     * <p>
     * newWorkStealingPool：一个拥有多个任
     * <p>
     * 关闭线程池可以调用shutdownNow和shutdown两个方法来实现
     */

    /**
     * shutdown：当我们调用shutdown后，线程池将不再接受新的任务，但也不会去强制终止已经提交或者正在执行中的任务
     */
    public void testShutDown1() throws InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(1);
        for (int i = 0; i < 5; i++) {
            System.err.println(i);
            pool.execute(() -> {
                try {
                    Thread.sleep(30000);
                    System.out.println("--");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        Thread.sleep(1000);
        pool.shutdown();
        pool.execute(() -> {
            try {
                Thread.sleep(30000);
                System.out.println("--");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * 还有一些业务场景下需要知道线程池中的任务是否全部执行完成，
     * 当我们关闭线程池之后，可以用isTerminated来判断所有的线程是否执行完成，
     * 千万不要用isShutdown，isShutdown只是返回你是否调用过shutdown的结果。
     */
    public void testShutDown2() throws InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(1);
        for (int i = 0; i < 5; i++) {
            System.err.println(i);
            pool.execute(() -> {
                try {
                    Thread.sleep(3000);
                    System.out.println("--");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        Thread.sleep(1000);
        pool.shutdown();
        while (true) {
            if (pool.isTerminated()) {
                System.out.println("所有的子线程都结束了！");
                break;
            }
            Thread.sleep(1000);
        }
    }

    /**
     * 在实际的使用过程中，大部分我们都是用Executors去创建线程池直接使用，
     * 如果有一些其他的需求，比如指定线程池的拒绝策略，阻塞队列的类型，线程名称的前缀等等，我们可以采用自定义线程池的方式来解决。
     * <p>
     * 如果只是简单的想要改变线程名称的前缀的话可以自定义ThreadFactory来实现，在Executors.new…中有一个ThreadFactory的参数，如果没有指定则用的是DefaultThreadFactory。
     * <p>
     * 下面我们看下ThreadPoolExecutor构造函数的定义：
     * public ThreadPoolExecutor(int corePoolSize,
     * int maximumPoolSize,
     * long keepAliveTime,
     * TimeUnit unit,
     * BlockingQueue<Runnable> workQueue,
     * ThreadFactory threadFactory,
     * RejectedExecutionHandler handler);
     * corePoolSize
     * 线程池大小，决定着新提交的任务是新开线程去执行还是放到任务队列中，也是线程池的最最核心的参数。一般线程池开始时是没有线程的，只有当任务来了并且线程数量小于corePoolSize才会创建线程。
     * maximumPoolSize
     * 最大线程数，线程池能创建的最大线程数量。
     * keepAliveTime
     * 在线程数量超过corePoolSize后，多余空闲线程的最大存活时间。
     * unit
     * 时间单位
     * workQueue
     * 存放来不及处理的任务的队列，是一个BlockingQueue。
     * threadFactory
     * 生产线程的工厂类，可以定义线程名，优先级等。
     * handler
     * 拒绝策略，当任务来不及处理的时候，如何处理, 前面有讲解。
     * <p>
     * 我这边用ArrayBlockingQueue替换了LinkedBlockingQueue，指定了队列的大小，当任务超出队列大小之后使用CallerRunsPolicy拒绝策略处理。
     * <p>
     * 这样做的好处是严格控制了队列的大小，不会出现一直往里面添加任务的情况，有的时候任务处理的比较慢，任务数量过多会占用大量内存，导致内存溢出。
     * <p>
     * 当然你也可以在提交到线程池的入口进行控制，比如用CountDownLatch, Semaphore等。
     */
    public static class FangjiaThreadPoolExecutor {
        private static ExecutorService executorService = newFixedThreadPool(50);

        public static void main(String[] args) {
            for (int i = 0; i < 100; i++) {
                FangjiaThreadPoolExecutor.execute(() -> System.out.println(Thread.currentThread().getName() + "开始发车啦...."));
            }
        }

        private static ExecutorService newFixedThreadPool(int nThreads) {
            return new ThreadPoolExecutor(nThreads, nThreads, 0L, TimeUnit.MILLISECONDS,
                    new ArrayBlockingQueue<>(10000), new DefaultThreadFactory(), new ThreadPoolExecutor.CallerRunsPolicy());
        }

        public static void execute(Runnable command) {
            executorService.execute(command);
        }

        public static void shutdown() {
            executorService.shutdown();
        }

        static class DefaultThreadFactory implements ThreadFactory {
            private static final AtomicInteger poolNumber = new AtomicInteger(1);
            private final ThreadGroup group;
            private final AtomicInteger threadNumber = new AtomicInteger(1);
            private final String namePrefix;

            DefaultThreadFactory() {
                SecurityManager s = System.getSecurityManager();
                group = (s != null) ? s.getThreadGroup() :
                        Thread.currentThread().getThreadGroup();
                namePrefix = "FSH-pool-" +
                        poolNumber.getAndIncrement() +
                        "-thread-";
            }

            public Thread newThread(Runnable r) {
                Thread t = new Thread(group, r,
                        namePrefix + threadNumber.getAndIncrement(),
                        0);
                if (t.isDaemon())
                    t.setDaemon(false);
                if (t.getPriority() != Thread.NORM_PRIORITY)
                    t.setPriority(Thread.NORM_PRIORITY);
                return t;
            }
        }
    }

    class Data {
        String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    class MyRunnable implements Runnable {
        private Data data;

        public MyRunnable(Data data) {
            this.data = data;
        }

        @Override
        public void run() {
            data.setName("yinjihuan");
        }
    }


}
