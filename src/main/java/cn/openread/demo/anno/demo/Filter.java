package cn.openread.demo.anno.demo;

/**
 * demo
 *
 * @author simon
 * @create 2018-06-15 上午10:39
 **/
@Table("user")
public class Filter {
    @Column("id")
    private Long id;

    @Column("user_name")
    private String userName;

    @Column("email")
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
