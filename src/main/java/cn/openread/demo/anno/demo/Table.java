package cn.openread.demo.anno.demo;

import java.lang.annotation.*;

//1.注解作用域 link ElementType.java
@Target({ElementType.TYPE})
//2.声明周期范围
@Retention(RetentionPolicy.RUNTIME)
//4.生javadoc会包含java信息
@Documented
public @interface Table {
    String value() default "";
}
