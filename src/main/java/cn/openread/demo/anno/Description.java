package cn.openread.demo.anno;


import java.lang.annotation.*;

//元注解

//1.注解作用域 link ElementType.java
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.TYPE_USE})
//2.声明周期范围
@Retention(RetentionPolicy.RUNTIME)
//3.运行子类继承
@Inherited
//4.生javadoc会包含java信息
@Documented

/**
 * 注解
 * 1.无参数
 * 2.无异常
 * 3.返回类型仅仅允许8种基本数据类型
 * 4.只有一个成员只能是value,约定
 * 5.没有成员的注解是标识注解
 * */
public @interface Description {

    String desc();

    String author();

    int age() default 18;
}
