package cn.openread.demo.anno;

/**
 * demo
 *
 * @author simon
 * @create 2018-06-15 上午9:45
 **/
@Description(desc = "这是个Class", author = "Simon", age = 24)
public class Person {

    @Description(desc = "这是个Method", age = 20, author = "simon")
    public String name() {
        return "";
    }


    public int age() {
        return 1;
    }

    @Deprecated
    public void sing() {
    }
}
