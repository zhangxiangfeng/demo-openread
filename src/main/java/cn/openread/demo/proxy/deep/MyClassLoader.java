package cn.openread.demo.proxy.deep;

/**
 * 自定义一个类加载器，用于将字节码转换为class对象
 *
 * @author simon
 * @create 2018-05-30 上午10:55
 **/
public class MyClassLoader extends ClassLoader {

    public Class<?> defineMyClass(byte[] b, int off, int len) {
        return super.defineClass(b, off, len);
    }

}
