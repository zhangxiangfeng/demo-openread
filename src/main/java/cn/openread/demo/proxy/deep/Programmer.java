package cn.openread.demo.proxy.deep;

/**
 * Programmer
 *
 * @author simon
 * @create 2018-05-30 上午10:54
 **/
public class Programmer {
    public void code() {
        System.out.println("I'm a Programmer,Just Coding.....");
    }
}
