package cn.openread.demo.proxy.jdk;

public interface HelloWorld {

    void sayHello(String name);

    void exit(String name);
}
