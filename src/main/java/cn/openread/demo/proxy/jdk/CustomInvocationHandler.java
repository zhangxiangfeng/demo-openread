package cn.openread.demo.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class CustomInvocationHandler implements InvocationHandler {
    private Object target;

    public CustomInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("开始调用:Before invocation" + method.toString());

        Object retVal = method.invoke(target, args);

        System.out.println("结束调用:After invocation" + method.toString());
        System.out.println("==============================================");
        return retVal;
    }

    @Override
    public String toString() {
        return target.toString();
    }
}
