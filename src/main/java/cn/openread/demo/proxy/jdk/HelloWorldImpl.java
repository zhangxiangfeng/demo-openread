package cn.openread.demo.proxy.jdk;

public class HelloWorldImpl implements HelloWorld {
    @Override
    public void sayHello(String name) {
        System.out.println("Hello " + name);
    }

    @Override
    public void exit(String name) {
        System.out.println("Bye " + name);
    }

    @Override
    public String toString() {
        System.out.println("toString");
        return super.toString();
    }
}
