package cn.openread.demo.jvm;

/**
 * static关键字理解
 * <p>
 * 1.static 方法内部不能调用非静态的，仅仅可以用通过类本身去调用，只要类被加载了，就可以通过类名去进行访问
 * 2.static 代码块来优化程序性能(涉及到类初始化的顺序)
 *
 * @author simon
 * @create 2018-09-05 上午11:48
 **/
public class StaticTest {

    static {
        System.out.println(2);
    }

    {
        System.out.println(1);
    }

    public static void main(String[] args) {
        StaticTest s1 = new StaticTest();
        System.out.println(s1.toString());

        StaticTest s2 = new StaticTest();
        System.out.println(s2.toString());
    }

    @Override
    public String toString() {
//        System.out.println(3);
        return "3";
    }
}
