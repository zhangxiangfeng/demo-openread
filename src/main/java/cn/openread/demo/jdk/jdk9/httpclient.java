package cn.openread.demo.jdk.jdk9;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * 9自带httpclient访问api
 *
 * @author simon
 * @create 2018-02-26 上午9:55
 **/
public class httpclient {

    public static void main(String[] args) {
        try {
            HttpClient client = HttpClient.newBuilder().build();

            HttpRequest request = HttpRequest.newBuilder(new URI("https://www.baidu.com")).GET().build();

            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandler.asString());

            System.out.println(response.statusCode());
            System.out.println(response.version().name());
            System.out.println(response.body());
            response.headers().map().keySet().forEach(System.out::println);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
