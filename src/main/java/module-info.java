module demo.openread {
//    可以导入module的名称
    requires jdk.incubator.httpclient;
//    也可以导入包的名称,兼容jdk9之前的jar
    requires cglib;
    requires org.objectweb.asm;
    requires javassist;
}