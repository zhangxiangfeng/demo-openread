# 一些经典源码的demo

- 1.JDK动态代理

> 预览图

```$xslt
src
├── main
│   ├── java
│   │   └── cn
│   │       └── openread
│   │           └── demo
│   │               └── proxy  JDK动态代理
│   │                   ├── CustomInvocationHandler.java
│   │                   ├── HelloWorldImpl.java
│   │                   └── HelloWorld.java
│   └── resources
└── test
    ├── java
    │   └── cn
    │       └── openread
    │           └── demo
    │               └── proxy JDK动态代理 测试
    │                   └── ProxyTest.java
    └── resources

```